﻿using PCSC;
using PCSC.Reactive;
using PCSC.Reactive.Events;
using System;
using System.Linq;
using ThaiNationalIDCard;

namespace ThaiSmartCardReader
{
    public class Program
    {
        private ThaiIDCard idcard = new ThaiIDCard();
        public static void Main()
        {
            var readers = GetReaders();

            if (!readers.Any())
            {
                Console.WriteLine("You need at least one connected smart card reader.");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Listen to all reader events. Press any key to stop.");

            var monitorFactory = MonitorFactory.Instance;

            var subscription = monitorFactory
                .CreateObservable(SCardScope.System, readers)
                .Subscribe(OnNext, OnError);

            Console.ReadKey();
            subscription.Dispose();
        }

        private static void OnError(Exception exception)
        {
            Console.WriteLine("ERROR: {0}", exception.Message);
        }

        private static void OnNext(MonitorEvent ev)
        {
            Console.WriteLine($"Event type {ev.GetType()}, reader: {ev.ReaderName}");
            var strType = ev.GetType().UnderlyingSystemType.Name;
            if (strType == "CardInserted")
            {
                new Reader();
            }
        }

        private static string[] GetReaders()
        {
            var contextFactory = ContextFactory.Instance;
            using (var ctx = contextFactory.Establish(SCardScope.System))
            {
                return ctx.GetReaders();
            }
        }
    }
    public partial class Reader
    {
        private ThaiIDCard idcard = new ThaiIDCard();
        public Reader()
        {
            try
            {
                Personal personal = idcard.readAll();
                if (personal != null)
                {
                    Console.WriteLine(personal.Citizenid);
                    iLocker ilock = new iLocker();
                    ilock.Send(personal.Citizenid);
                    ilock.Send(personal.Th_Firstname);
                    ilock.Send(personal.Th_Lastname);
                }
                else if (idcard.ErrorCode() > 0)
                {
                    Console.WriteLine(idcard.Error());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
